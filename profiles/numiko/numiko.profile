<?php

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function numiko_form_install_configure_form_alter(&$form, $form_state) {

  $form['site_information']['site_mail']['#default_value'] = "noreply@sitename.com";
  $form['admin_account']['account']['name']['#default_value'] = "sysadmin";
  $form['admin_account']['account']['mail']['#default_value'] = "dev@numiko.net";
  
}