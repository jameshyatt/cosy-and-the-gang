<div class="charms-left">
	<a href="#menu" id="menu" class="menu">Menu</a>
	<a href="<?php echo url('user');?>" class="user-account">User</a>
	<?php
	 print cosy_alt_block_render('locale', 'language');
	?>
</div>
<div class="charms-right">
<a href="<?php echo url('search');?>" class="search"><?php echo t('Search');?></a>
<a href="<?php echo url('cart');?>" class="basket-teaser"><?php echo $items;?> <span class="basket-helper"><?php echo t('in your basket');?></span></a>
</div>