<?php
  $vid = taxonomy_vocabulary_machine_name_load("collections")->vid;
  $terms = taxonomy_get_tree($vid);
  $total = count($terms);
?>
<section class="overlay cosy-overlay">
	<h2><span><?php echo t('Take a look around');?></span> <?php echo t('All collections');?></h2>

	<div class="container">

      <div class="grid-without-lines">
        <?php 
          $i = 0;
          foreach($terms as $term) {
            if (module_exists('i18n_taxonomy')) { //To not break your site if module is not installed
              $term_translated = i18n_taxonomy_localize_terms($term); // The important part!
            }
        ?>
        <div class="grid-teaser collection <?php echo $i == ($total - 2) ? 'collection-spacer' : '';?>">
          <div class="bleed">
            <a href="<?php echo url('collection/'.cosy_get_term_url_from_name($term->name));?>">
              <div class="image cat-<?php echo cosy_get_term_url_from_name($term->name);?>"></div>
              <span class="category-title title"><span><?php echo $term_translated->description;?> </span><?php echo $term_translated->name;?></span>
            </a>
          </div>
        </div>
        <?php $i++;} ?>
      </div>

      <?php
        print cosy_alt_block_render('menu', 'menu-footer');
      ?>
	</div>

</section>