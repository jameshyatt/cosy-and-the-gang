<section class="slice">
  <div class="container">
      <h2><span>Featured </span>Products</h2>
      <div class="grid-with-lines">
        <div class="grid-teaser">
          <div class="bleed">
            <div class="image">
              <img src="/sites/all/themes/cosy/library/images/tmp/product.jpg"/>
            </div>
          </div>
        </div>
        <div class="grid-teaser">
          <div class="bleed">
            <div class="image">
              <img src="/sites/all/themes/cosy/library/images/tmp/product_two.jpg"/>
            </div>
          </div>
        </div>
        <div class="grid-teaser">
          <div class="bleed">
            <div class="image">
              <img src="/sites/all/themes/cosy/library/images/tmp/product.jpg"/>
            </div>
          </div>
        </div>
        <div class="grid-teaser">
          <div class="bleed">
            <div class="image">
              <img src="/sites/all/themes/cosy/library/images/tmp/product_two.jpg"/>
            </div>
          </div>
        </div>
        <div class="grid-teaser">
          <div class="bleed">
            <div class="image">
              <img src="/sites/all/themes/cosy/library/images/tmp/product.jpg"/>
            </div>
          </div>
        </div>
        <div class="grid-teaser">
          <div class="bleed">
            <div class="image">
              <img src="/sites/all/themes/cosy/library/images/tmp/product_two.jpg"/>
            </div>
          </div>
        </div>
        <div class="grid-teaser">
          <div class="bleed">
            <div class="image">
              <img src="/sites/all/themes/cosy/library/images/tmp/product.jpg"/>
            </div>
          </div>
        </div>
        <div class="grid-teaser">
          <div class="bleed">
            <div class="image">
              <img src="/sites/all/themes/cosy/library/images/tmp/product_two.jpg"/>
            </div>
          </div>
        </div>
      </div>
  </div> 
</section>
<section class="slice alternate">
  <div class="container">
      <h2><span>Product </span>Collections</h2>
      <div class="grid-without-lines">
        <div class="grid-teaser">
          <div class="bleed">
            <div class="image cat-new-things"></div>
            <span class="title"><span>All our </span>new things</span>
          </div>
        </div>
        <div class="grid-teaser">
          <div class="bleed">
            <div class="image cat-kitchen"></div>
            <span class="title"><span>Things for the </span>kitchen</span>
          </div>
        </div>
        <div class="grid-teaser">
          <div class="bleed">
            <div class="image cat-textile"></div>
            <span class="title"><span>Style & </span>textile</span>
          </div>
        </div>
        <div class="grid-teaser">
          <div class="bleed">
            <div class="image cat-print"></div>
            <span class="title"><span>Original </span>print goods</span>
          </div>
        </div>
        <div class="grid-teaser faux-teaser">
            <div class="bleed">
            </div>
        </div>
        <div class="grid-teaser">
          <div class="bleed">
            <div class="image cat-furniture"></div>
            <span class="title"><span>Cool & comfy </span>furniture</span>
          </div>
        </div>
        <div class="grid-teaser">
          <div class="bleed">
            <div class="image cat-lifestyle"></div>
            <span class="title"><span>Objects for your </span>lifestyle</span>
          </div>
        </div>
        </div>
      </div>
  </div>    
</section>

<section class="slice">
  <div class="container">
      <h2><span>We are </span>Cosy & The Gang</h2>
      <div class="grid-pages">
        <div class="grid-teaser">
            <div class="image">
              <img src="/sites/all/themes/cosy/library/images/tmp/page_blog.jpg"/>
            </div>
            <span class="medium-title">Latest from the blog</span>
        </div>
        <div class="grid-teaser">
            <div class="image">
              <img src="/sites/all/themes/cosy/library/images/tmp/page_about.jpg"/>
            </div>
            <span class="medium-title">About us</span>
        </div>
        <div class="grid-teaser">
            <div class="image">
              <img src="/sites/all/themes/cosy/library/images/tmp/page_terms.jpg"/>
            </div>
            <span class="medium-title">T&Cs</span>
        </div>
      </div>
  </div>
</section>