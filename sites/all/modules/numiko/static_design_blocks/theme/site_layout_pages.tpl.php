<h2><span><?php echo t('We are'); ?> </span><span class="cosy-tr"><?php echo t('Cosy & The Gang');?></span></h2>
<div class="grid-pages">
  <div class="grid-teaser page-teaser">
    <a href="http://cosyandthegang.tumblr.com">
      <div class="image" style="background-image:url('/sites/all/themes/cosy/library/images/tmp/promo-blog.jpg');">
        <img src="/sites/all/themes/cosy/library/images/tmp/page_blog.jpg"/>
      </div>
      <span class="medium-title"><?php echo t('Latest from the blog');?></span>
    </a>
  </div>
  <div class="grid-teaser page-teaser">
    <a href="<?php echo url('node/14');?>">
      <div class="image" style="background-image:url('/sites/all/themes/cosy/library/images/tmp/promo-about.jpg');">
        <img src="/sites/all/themes/cosy/library/images/tmp/page_about.jpg"/>
      </div>
      <span class="medium-title"><?php echo t('About us');?></span>
    </a>
  </div>
  <div class="grid-teaser page-teaser">
    <a href="<?php echo url('node/17');?>">
      <div class="image" style="background-image:url('/sites/all/themes/cosy/library/images/tmp/promo-terms.jpg');">
        <img src="/sites/all/themes/cosy/library/images/tmp/page_terms.jpg"/>
      </div>
      <span class="medium-title"><?php echo t('T&Cs');?></span>
    </a>
  </div>
</div>