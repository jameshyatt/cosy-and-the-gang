
        <footer class="footer">
            <div class="footer-inner">
                <div class="footer-col">
                  <p>&copy;<?php echo date("Y"); ?> Cosy &amp; The Gang.<br />
                  <a href="http://www.colingrist.com" target="_blank">Site by</a> + <a href="http://twitter.com/_jameshyatt" target="_blank">Build by</a> + <a href="http://hoodzpahdesign.com">With</a>.</p>
                </div>
                <div class="footer-col">
                  <p>Get the Newsletter?<br />
                  <a href="#" class="js-subscribe">Subscribe.</a></p>
                </div>
                <div class="footer-col -social">
                  <ul class="footer-social social-icons">
                    <li><a href="https://www.instagram.com/cosyandthegang/" class="instagram">Instagram</a></li>
                    <li><a href="http://fr.pinterest.com/cosyandthegang/" class="pinterest">Pinterest</a></li>
                    <li><a href="https://www.facebook.com/cosyandthegang?fref=ts" class="facebook">Facebook</a></li>
                  </ul>
                </div>
            </div>
        </footer>
