<?php
/**
 * @file
 * Defines an 'flat rate shipping field method' to allow a simple, flat rate price field 
 * to be used to calculate a product's shipping price.
 */
function commerce_shipping_field_field_attach_create_bundle($entity_type, $bundle){
  if ($entity_type != 'commerce_product') {
    return;
  }
  /*Check the field exists.*/
  if (!field_info_instance('commerce_product', 'commerce_shipping_price', $bundle)) {
   commerce_price_create_instance('commerce_shipping_price', 'commerce_product', $bundle, t('Shipping Price'));
  }
}

/**
 * Implements hook_commerce_shipping_method_info().
 */
function commerce_shipping_field_commerce_shipping_method_info() {
  $shipping_methods = array();

  $shipping_methods['commerce_shipping_field_method'] = array(
    'title' => t('Product Shipping Price Field Method'),
    'description' => t('Defines a single flat rate service with a couple of service details.'),
  );

  return $shipping_methods;
}

/**
 * Implements hook_commerce_shipping_service_info().
 */
function commerce_shipping_field_commerce_shipping_service_info() {
  $shipping_services = array();

  $shipping_services['commerce_shipping_field_service'] = array(
    'title' => t('Field-Driven Shipping Service'),
    'description' => t('You will receive a tracking number as soon as your parcel is on its way.'),
    'display_title' => t('Courier shipping'),
    'shipping_method' => 'commerce_shipping_field_method',
    'price_component' => 'shipping',
    'callbacks' => array(
      'rate' => 'commerce_shipping_field_service_rate',
    ),
  );

  return $shipping_services;
}

/**
 * Shipping service callback: returns a base price array for a shipping service
 * calculated for the given order.
 */
function commerce_shipping_field_service_rate($shipping_service, $order) {  
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $line_items = $order_wrapper->commerce_line_items;
  $shipping = $order_wrapper->commerce_customer_shipping->commerce_customer_address->value();
  $country_code = $shipping['country'];
 
  $zone = commerce_shipping_field_get_zone($country_code);

  $real_weight = 0;
  $volumetric_weight = 0;
  $amount = 0;

  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {

    $line_item = $line_item_wrapper->value();
    $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

    if ( isset($line_item->commerce_product)) {
      $line_item = commerce_line_item_load($line_item->line_item_id);
      $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

      $item_weight = $line_item_wrapper->commerce_product->field_real_weight->weight->value() * $line_item_wrapper->quantity->raw();
      $real_weight = $item_weight + $real_weight;

      // Get the minimum box size this product will fit.
      $volumetric_max_weight = intval($line_item_wrapper->commerce_product->field_minimum_box_size->weight->value());

      // If the volumetric weight is more than 0.
      if($volumetric_max_weight > 0) {

        // Get how many can fit in a box
        $volumetric_max_in_box = intval($line_item_wrapper->commerce_product->field_maximum_products_in_a_box->value());
        
        $volumetric_weight += $volumetric_max_weight;
 
        for ($counter = 1; $counter < $line_item_wrapper->quantity->raw(); $counter++ ) {
          if ($counter % $volumetric_max_in_box == 0) {
                $volumetric_weight += $volumetric_max_weight;
          }  
        }
      } else {
          $item_volumetric_weight = $line_item_wrapper->commerce_product->field_volumetric_weight->weight->value() * $line_item_wrapper->quantity->raw();
          $volumetric_weight = $item_volumetric_weight + $volumetric_weight;
      }
      
    }

  }

  watchdog('debug', "The real weight is :" . $real_weight);
  watchdog('debug', "The volumetric weight is :" . $volumetric_weight);

  /* If Zone_domestic, Zone_1 or Zone 2, use the real weight */
  if(commerce_shipping_field_use_real_weight($zone)) {
    $weight = $real_weight;
  } else {
    $weight = max($real_weight, $volumetric_weight);
  }

  // krumo($real_weight);
  // krumo($volumetric_weight);

  $price = commerce_shipping_field_get_price($country_code, $weight, $zone);

  watchdog('debug', "The shipping weight is :" . $weight);

  return array(
    'amount' => $price,
    'currency_code' => $order_wrapper->commerce_order_total->currency_code->value(),
    'data' => array(),
  );
}

function commerce_shipping_field_get_zone($code) {
  $code = strtoupper($code);

  $codes = array('FR');
  if(in_array($code, $codes)) {
    return 'ZONE_DOMESTIC';
  }
  $codes = array('AD', 'AT', 'BE', 'DE', 'IE', 'IT', 'LU', 'NL', 'PT', 'ES', 'GB', 'CH', 'LI');
  if(in_array($code, $codes)) {
    return 'ZONE_1';
  }
  $codes = array('BG', 'HR', 'CY', 'CZ', 'DK', 'EE', 'FI', 'GR', 'HU', 'LV', 'LT', 'MT', 'PL', 'RO', 'RS', 'SE', 'SK', 'SI');
  if(in_array($code, $codes)) {
    return 'ZONE_2';
  }
  $codes = array('CA', 'US', 'NO', 'IC');
  if(in_array($code, $codes)) {
    return 'ZONE_3';
  }
  return 'ZONE_REST';
}

function commerce_shipping_field_get_price($country_code, $weight, $zone) {
  $rates = array(
    ZONE_DOMESTIC => array(1 => 9.00, 3 => 12.00, 5 => 15.00, 10 => 21.00, 30 => 30.00),
    ZONE_1 => array(1 => 15.00, 3 => 15.00, 5 => 21.00, 10 => 30.00, 30 => 35.00),
    ZONE_2 => array(1 => 18.00, 3 => 18.00, 5 => 25.00, 10 => 35.00, 30 => 45.00),
    ZONE_3 => array(1 => 21.00, 3 => 21.00, 5 => 35.00, 10 => 45.00, 30 => 50.00),
    ZONE_REST => array(1 => 45.00, 3 => 45.00, 5 => 60.00, 10 => 95.00, 30 => 150.00)
  );

  $price = commerce_shipping_field_closest_price($rates[$zone], $weight);

  return $price * 100;
}

function commerce_shipping_field_closest_price($array, $number) {
    foreach ($array as $a => $value) {
        if ($a >= $number) return $value;
    }
    return end($array); // or return NULL;
}

function commerce_shipping_field_use_real_weight($zone) {
  if($zone == "ZONE_DOMESTIC" || $zone == "ZONE_1" || $zone == "ZONE_2") {
    return true;
  }
  return false;
}