<?php
/**
 * @file
 * Provides overrides and additions to aid the theme.
 */

/**
 * Implements hook_css_alter().
 *
 * Remove all the unused css files.
 */
function cosy_css_alter(&$css) {

  /* Remove some default Drupal css */
  $exclude = array(
    'modules/aggregator/aggregator.css' => FALSE,
    'modules/block/block.css' => FALSE,
    'modules/book/book.css' => FALSE,
    'modules/comment/comment.css' => FALSE,
    'modules/dblog/dblog.css' => FALSE,
    'modules/field/theme/field.css' => FALSE,
    'modules/file/file.css' => FALSE,
    'modules/filter/filter.css' => FALSE,
    'modules/forum/forum.css' => FALSE,
    'modules/help/help.css' => FALSE,
    'modules/menu/menu.css' => FALSE,
    'modules/node/node.css' => FALSE,
    'modules/openid/openid.css' => FALSE,
    'modules/poll/poll.css' => FALSE,
    'modules/profile/profile.css' => FALSE,
    'modules/search/search.css' => FALSE,
    'modules/statistics/statistics.css' => FALSE,
    'modules/syslog/syslog.css' => FALSE,
    'modules/system/admin.css' => FALSE,
    'modules/system/maintenance.css' => FALSE,
    'modules/system/system.css' => FALSE,
    'modules/system/system.admin.css' => FALSE,
    'modules/system/system.maintenance.css' => FALSE,
    'modules/system/system.messages.css' => FALSE,
    'modules/system/system.theme.css' => FALSE,
    'modules/system/system.menus.css' => FALSE,
    'modules/taxonomy/taxonomy.css' => FALSE,
    'modules/tracker/tracker.css' => FALSE,
    'modules/update/update.css' => FALSE,
    'modules/user/user.css' => FALSE,
  );
  $css = array_diff_key($css, $exclude);
}

function cosy_preprocess_html(&$variables) {
  if(arg(0) == "user" && user_is_logged_in() == false) {
    $variables['classes_array'][] = 'user-panel';
  }
  if(arg(0) == "user" && arg(1) == null) {
    $variables['classes_array'][] = 'user-login';
  }
  if(arg(0) == "user" && arg(1) == "register") {
    $variables['classes_array'][] = 'user-register';
  }
  if(arg(0) == "user" && arg(1) == "password") {
    $variables['classes_array'][] = 'user-password';
  }
  if(arg(0) == "user" && arg(1) == "account") {
    $variables['classes_array'][] = 'user-account';
  }
}

/**
 * Implements hook_form_FORM_ID_alter(&$form, &$form_state, $form_id)
 */
function cosy_form_views_form_commerce_cart_form_default_alter(&$form, &$form_state, $form_id) {

  $form['actions']['continue_shopping'] = array(
    '#type' => 'button',
    '#value' => t('Continue Shopping'),
    '#weight' => -999,
  );

  $form['actions']['continue_shopping']['#attributes'] = array('ONCLICK' => "window.location.href='/'; return false;");


  if(isset($form['edit_quantity'])) {
      foreach($form['edit_quantity'] as $index => $input ) {
        if(isset($input["#type"])) {
         //$form['edit_quantity'][0]["#type"] = "number"; 
        }
      }
  }
}

function cosy_form_alter(&$form, $form_state, $form_id) {
  switch ($form_id)  {
  case 'commerce_checkout_form_payment_stripe':
    $form['buttons']['continue']['#value'] = t('Pay');
  break;
  }
}

function cosy_preprocess_page(&$variables) {
  if(arg(0) == "user" && user_is_logged_in() == true) {

    $local_tabs = render(menu_local_tabs());
    $local_actions = render(menu_local_actions());


    if(strlen($local_actions)) {
      $variables['page']['content']['system_main']['action_links'] = array(
        '#weight' => -10,
        '#markup' => '<nav class="action-nav">
          <ul>'.$local_actions.'</ul>
          </nav>',
      );
    }

    if(strlen($local_tabs)) {
      $variables['page']['content']['system_main']['action'] = array(
        '#weight' => -10,
        '#markup' => '<nav class="user-nav">
          <ul>'.$local_tabs.'</ul>
          </nav>',
      );
    } else {
      $variables['page']['content']['system_main']['action'] = array(
        '#weight' => -10,
        '#markup' => '<nav class="user-nav">
          <ul><li><a href="/user">'.t('Back to dashboard').'</a></li></ul>
          </nav>',
      );
    }

  }

}

// function cosy_username_alter(&$name, $account) {
//   if (isset($account->uid)) {
//     $this_user = user_load($account->uid);//loads the custom fields
//     $name = "";
//   }
// }

?>