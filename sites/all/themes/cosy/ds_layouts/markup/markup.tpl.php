<?php
/**
 * @file
 * Display Suite Markup
 */
?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="ds-markup <?php print $classes;?> clearfix">

  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <<?php print $enabled_wrapper ?> class="group-enabled<?php print $enabled_classes; ?>">
    <?php print $enabled; ?>
  </<?php print $enabled_wrapper ?>>

</<?php print $layout_wrapper ?>>

<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>