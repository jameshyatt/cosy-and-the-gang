<?php
function ds_flexible_no_content() {
  return array(
    'label' => t('Flexible - Content not displayed'),
    'regions' => array(
      'blocks' => t('Fields enabled as blocks')
    )
  );
}
?>