<?php
/**
 * @file
 * Display Suite Markup
 */
 unset($content['links']['comment']['#links']['comment-add']);
?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="ds-markup <?php print $classes;?> clearfix">

  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <<?php print $enabled_wrapper ?> class="slice group-enabled<?php print $enabled_classes; ?>">

  	<?
  	/**
  	* We want to just output the body at the top of pages with this layout. 
  	* node level blocks will then take care of layering slices.
  	*/
  	?>
    
    <div class="full-width-content">
      <?php echo render($content['body']); ?>
    </div>

  </<?php print $enabled_wrapper ?>>

  </<?php print $layout_wrapper ?>>



<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>