<?php

/**
 * @file
 * Display Suite Flexible with Content
 */
?>
<div class="design-council-block">
	<section class="slice <?php print $classes;?> clearfix">

	  <?php if (isset($title_suffix['contextual_links'])): ?>
	  <?php print render($title_suffix['contextual_links']); ?>
	  <?php endif; ?>

	  <<?php print $main_wrapper ?> class="<?php print $main_classes; ?>">
	    <?php print $main; ?>
	  </<?php print $main_wrapper ?>>

	</section>
</div>
<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>