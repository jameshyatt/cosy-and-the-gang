<?php
function ds_flexible_content() {
  return array(
    'label' => t('Flexible - Content displayed'),
    'regions' => array(
      'main' => t('Content'),
      'blocks' => t('Fields enabled as blocks')
    )
  );
}
?>