'use strict';

/*
* Notes and plugins used
* Spliting tasks into seperate files: http://macr.ae/article/splitting-gulpfile-multiple-files.html
* [1] Loading plugins automatically: https://github.com/jackfranklin/gulp-load-plugins
*
*/

/*
* Plugin Requires
*/
var gulp = require('gulp');

/*
* Load all the plugins into the variable $
*/
var $ = require('gulp-load-plugins')({
  pattern: '*',
});

/*
* Pass the plugins along so that your tasks can use them
* Will load all xxx.tasks.js files
*/
$.loadSubtasks('tasks/', $);


/*
* Combination tasks
*/
gulp.task('copy', ['copy:fonts', 'copy:svgs', 'copy:videos', 'copy:js']);
gulp.task('images',  function(cb){
   $.runSequence('images:sprite', cb);
});
gulp.task('clean', ['clean:dist']);

/*
* Watch Task
*/
gulp.task('dev', function() {
  gulp.watch('library/stylesheets/sass/**/*.scss',['css']);
});

/*
*  Full build
*/
gulp.task('default', function(cb){
  $.runSequence(['css'], cb);
});

/*
*  Aliases
*/
gulp.task('styles', ['css']);
gulp.task('scripts', ['js']);
gulp.task('serve', ['browser-sync']);