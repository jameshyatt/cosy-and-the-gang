<?php
	$term_name = cosy_get_term_name_from_url_sgement(1, true);
	$term_name_english = cosy_get_term_name_from_url_sgement_en(1, false);
?>
	<div class="section-title with-icon collection-<?php echo cosy_string_as_css_class($term_name_english);?>">
		<h2><?php echo $term_name;?> <span><?php echo count($rows); ?> <?php echo t("products");?></span></h2>
	</div>
      <div class="grid-with-lines">
		<?php foreach ($rows as $id => $row): ?>
		    <?php print $row; ?>
		<?php endforeach; ?>
      </div>