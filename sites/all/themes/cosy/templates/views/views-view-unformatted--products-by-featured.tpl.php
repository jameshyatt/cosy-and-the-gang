<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php 
$group_nr = 1;                  // first group number
$last_row = count($rows) -1;    // last row
$wrapper  = 8;                  // put a wrapper around every 3 rows
?>
<?php foreach ($rows as $id => $row): ?>
<?php if ($id % $wrapper == 0) {print '<div class="grid-with-lines carousel clearfix grid-'.$group_nr.'">'; $i = 0; $group_nr++; } ?>
<?php print $row; ?>
<?php $i++; if ($i == $wrapper || $id == $last_row) print '</div>'; ?>
<?php endforeach; ?>