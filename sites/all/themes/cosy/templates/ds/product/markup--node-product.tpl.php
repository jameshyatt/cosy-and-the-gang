<?php
	$collection = render($content["field_collection"]);
	$collection_default = render($content["collection_english"]);
	$collection_url = cosy_get_term_url_from_name($collection_default);

	$display = array(
	  'label'=>'hidden',
	  'type'=>'image',
	  'settings' => array(
	    'image_style' => 'sqaure'
	  )
	);
	$thumbs = field_view_field('node', $node, 'field_product_images', $display);
	$thumbs = render($thumbs);
	$thumbsHTML = "";

	$display = array(
	  'label'=>'hidden',
	  'type'=>'image',
	  'settings' => array(
	    'image_style' => 'product'
	  )
	);
	$full = field_view_field('node', $node, 'field_product_images', $display);
	$full = render($full);	

	
	$thumbs_split = (explode(">", $thumbs));
	$full_split = (explode(">", $full));

	$doc = new DOMDocument();
	libxml_use_internal_errors(true);
	$doc->loadHTML( $full );
	$xpath = new DOMXPath($doc);
	$imgs = $xpath->query("//img");
	for ($i=0; $i < $imgs->length; $i++) {
	    $img = $imgs->item($i);
	    $src[] = $img->getAttribute("src");
	}
	
	

	foreach($thumbs_split as $index => $item) {
		if(strlen($item) > 0) {
			$thumbsHTML .= '<div class="product-view__thumbnail"><a href="'.$src[$index].'">'.$item.'></a></div>';
		}
	}

?>

<div class="section-title with-icon collection-<?php echo cosy_string_as_css_class($collection);?>">
	<h2><?php echo $title;?> <span>in <a href="<?php echo url("collection/$collection_url"); ?>">
		<?php echo $collection;?>
	</a>
	</span></h2>
</div>


<div class="product-view cf">
	<div class="product-view__left">
		<div class="product-view__main-image">
			<?php echo render($content["field_product_images"]);?>
		</div>
		<div class="product-view__main-thumbnails">
			<div class="wrapper">
				<?php echo $thumbsHTML;?>
			</div>
		</div>
	</div>
	<div class="product-view__right">
		<div class="product-view__body">
			<?php echo render($content["body"]);?>
		</div>
		<div class="product-view__options cf">
			<div class="product-full__price"><?php echo render($content["product:commerce_price"]);?></div>
			<div class="product-full__addtocart"><?php echo render($content["field_product_variations"]);?></div>
		</div>
		<div class="product-view__share">
			<h3 class="title-as-copy"><?php echo t("Share the love");?></h3>

			<div class="social-icons">
				<span class='st_facebook_large' displayText='Facebook'></span>
				<span class='st_twitter_large' displayText='Tweet'></span>
				<span class='st_pinterest_large' displayText='Pinterest'></span>
			</div>

		</div>
	</div>	
</div>
