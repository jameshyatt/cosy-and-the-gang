<div class="grid-teaser product-teaser">
  <div class="bleed">
    <div class="image">
      	<a class="product-teaser__link" href="<?php echo $node_url;?>">
	     	  <?php echo render($content["field_product_images"]);?>
          <?php echo render($content["field_product_state"]);?>
	        <div class="product-teaser__information teaser-overlay">
	        	<h2 class="product-teaser__title"><span class="product-teaser__collection"><?php echo render($content["field_collection"]);?></span><?php echo render($content["title"]);?></h2>
	        	<div class="product-teaser__price"><?php echo render($content["product:commerce_price"]);?></div>
	        </div>
  	  	</a>
    </div>
  </div>
</div>