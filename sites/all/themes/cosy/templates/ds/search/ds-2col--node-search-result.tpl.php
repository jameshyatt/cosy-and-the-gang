<article <?php print $layout_attributes; ?> class="ds-2col <?php print $classes;?> clearfix">
	<div class="group-left">
		<a href="<?php echo $node_url;?>"><?php echo render($content["field_product_images"]);?></a>
	</div>
	<div class="group-right">
		<h3><a href="<?php echo $node_url;?>"><?php echo $title;?></a></h3>
		<p><?php echo render($content["search_snippet"]);?></p>
	</div>
</article>