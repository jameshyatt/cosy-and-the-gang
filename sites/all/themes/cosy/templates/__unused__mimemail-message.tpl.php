<?php

/**
 * @file
 * Default theme implementation to format an HTML mail.
 *
 * Copy this file in your default theme folder to create a custom themed mail.
 * Rename it to mimemail-message--[module]--[key].tpl.php to override it for a
 * specific mail.
 *
 * Available variables:
 * - $recipient: The recipient of the message
 * - $subject: The message subject
 * - $body: The message body
 * - $css: Internal style sheets
 * - $module: The sending module
 * - $key: The message identifier
 *
 * @see template_preprocess_mimemail_message()
 */
?>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php if ($css): ?>
    <style type="text/css">
      <!--
      <?php print $css ?>
      -->
    </style>
    <?php endif; ?>
  </head>
<body id="mimemail-body" <?php if ($module && $key): print 'class="'. $module .'-'. $key .'"'; endif; ?> style="margin: 0px; padding: 0px;">

  <table width="600" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" border="0">
    <tr>
      <td width="600" valign="top" bgcolor="#ffffff"><img src="http://master-7rqtwti-nnq2ieweybj5m.eu.platform.sh/cosy-email/images/spacer.gif" width="600" height="40" alt="" style="display: block;" /></td>
    </tr>

    <tr>

      <td align="left" valign="top">

        <table width="600" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" border="0">
          <tr>
            <td width="1" valign="top" bgcolor="#ece9e2"><img src="http://master-7rqtwti-nnq2ieweybj5m.eu.platform.sh/cosy-email/images/spacer.gif" width="1" height="1" alt="" style="display: block;" /></td>
            <td width="598" colspan="3" valign="top" bgcolor="#ece9e2"><img src="http://master-7rqtwti-nnq2ieweybj5m.eu.platform.sh/cosy-email/images/spacer.gif" width="598" height="1" alt="" style="display: block;" /></td>
            <td width="1" valign="top" bgcolor="#ece9e2"><img src="http://master-7rqtwti-nnq2ieweybj5m.eu.platform.sh/cosy-email/images/spacer.gif" width="1" height="1" alt="" style="display: block;" /></td>
          </tr>

          <!-- Banner area -->

          <tr>
            <td width="1" valign="top" bgcolor="#ece9e2"><img src="http://master-7rqtwti-nnq2ieweybj5m.eu.platform.sh/cosy-email/images/spacer.gif" width="1" height="1" alt="" style="display: block;" /></td>
            <td width="598" colspan="3" valign="top" bgcolor="#ece9e2"><a href="http://www.cosyandthegang.com"><img src="http://master-7rqtwti-nnq2ieweybj5m.eu.platform.sh/cosy-email/images/banner.gif" width="598" height="230" alt="Cosy and The Gang" border="0" style="display: block; border: 0;" /></a></td>
            <td width="1" valign="top" bgcolor="#ece9e2"><img src="http://master-7rqtwti-nnq2ieweybj5m.eu.platform.sh/cosy-email/images/spacer.gif" width="1" height="1" alt="" style="display: block;" /></td>
          </tr>

          <!-- End Banner slice -->

          <tr>
            <td width="1" valign="top" bgcolor="#ece9e2"><img src="http://master-7rqtwti-nnq2ieweybj5m.eu.platform.sh/cosy-email/images/spacer.gif" width="1" height="1" alt="" style="display: block;" /></td>
            <td width="598" colspan="3" valign="top" bgcolor="#ffffff"><img src="http://master-7rqtwti-nnq2ieweybj5m.eu.platform.sh/cosy-email/images/spacer.gif" width="598" height="40" alt="" style="display: block;" /></td>
            <td width="1" valign="top" bgcolor="#ece9e2"><img src="http://master-7rqtwti-nnq2ieweybj5m.eu.platform.sh/cosy-email/images/spacer.gif" width="1" height="1" alt="" style="display: block;" /></td>
          </tr>

          <!-- Content slice -->

          <tr>
            <td width="1" valign="top" bgcolor="#ece9e2"><img src="http://master-7rqtwti-nnq2ieweybj5m.eu.platform.sh/cosy-email/images/spacer.gif" width="1" height="1" alt="" style="display: block;" /></td>
            <td width="40" valign="top" bgcolor="#ffffff"><img src="http://master-7rqtwti-nnq2ieweybj5m.eu.platform.sh/cosy-email/images/spacer.gif" width="40" height="1" alt="" style="display: block;" /></td>

            <td width="522" valign="top" bgcolor="#ffffff">

              <div style="color: #666666; font-size: 16px; line-height: 24px; font-family: georgia, times, serif;">
                <?php print $body ?>
              </div>

            </td>

            <td width="40" valign="top" bgcolor="#ffffff"><img src="http://master-7rqtwti-nnq2ieweybj5m.eu.platform.sh/cosy-email/images/spacer.gif" width="40" height="1" alt="" style="display: block;" /></td>
            <td width="1" valign="top" bgcolor="#ece9e2"><img src="http://master-7rqtwti-nnq2ieweybj5m.eu.platform.sh/cosy-email/images/spacer.gif" width="1" height="1" alt="" style="display: block;" /></td>
          </tr>
          <tr>
            <td width="1" valign="top" bgcolor="#ece9e2"><img src="http://master-7rqtwti-nnq2ieweybj5m.eu.platform.sh/cosy-email/images/spacer.gif" width="1" height="1" alt="" style="display: block;" /></td>
            <td width="598" colspan="3" valign="top" bgcolor="#ffffff"><img src="http://master-7rqtwti-nnq2ieweybj5m.eu.platform.sh/cosy-email/images/spacer.gif" width="598" height="40" alt="" style="display: block;" /></td>
            <td width="1" valign="top" bgcolor="#ece9e2"><img src="http://master-7rqtwti-nnq2ieweybj5m.eu.platform.sh/cosy-email/images/spacer.gif" width="1" height="1" alt="" style="display: block;" /></td>
          </tr>

          <!-- End Table slice -->

          <tr>
            <td width="1" valign="top" bgcolor="#ece9e2"><img src="http://master-7rqtwti-nnq2ieweybj5m.eu.platform.sh/cosy-email/images/spacer.gif" width="1" height="1" alt="" style="display: block;" /></td>
            <td width="598" colspan="3" valign="top" bgcolor="#ece9e2"><img src="http://master-7rqtwti-nnq2ieweybj5m.eu.platform.sh/cosy-email/images/spacer.gif" width="598" height="1" alt="" style="display: block;" /></td>
            <td width="1" valign="top" bgcolor="#ece9e2"><img src="http://master-7rqtwti-nnq2ieweybj5m.eu.platform.sh/cosy-email/images/spacer.gif" width="1" height="1" alt="" style="display: block;" /></td>
          </tr>
        </table>

      </td>

    </tr>

    <tr>
      <td width="600" colspan="3" valign="top" bgcolor="#ffffff"><img src="http://master-7rqtwti-nnq2ieweybj5m.eu.platform.sh/cosy-email/images/spacer.gif" width="600" height="40" alt="" style="display: block;" /></td>
    </tr>


    <tr>
      <td width="600" colspan="3" valign="top" bgcolor="#ffffff"><font size="2" face="georgia,times,serif" color="#666666" style="color: #666666; font-size: 14px; line-height: 20px; font-family: georgia, times, serif;"><em>If you have any questions, please contact us at: <a href="mailto:info@cosyandthegang.com" style="text-decoration: underline;"><font size="2" face="georgia,times,serif" color="#666666" style="color: #666666; font-size: 14px; line-height: 20px; font-family: georgia, times, serif;">info@cosyandthegang.com</font></a></em></font></td>
    </tr>

    <tr>
      <td width="600" colspan="3" valign="top" bgcolor="#ffffff"><img src="http://master-7rqtwti-nnq2ieweybj5m.eu.platform.sh/cosy-email/images/spacer.gif" width="600" height="40" alt="" style="display: block;" /></td>
    </tr>
  </table>

</body>
</html>