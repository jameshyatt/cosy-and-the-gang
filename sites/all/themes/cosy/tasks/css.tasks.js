/*
* Available tasks
* -----------------
* css - Run sass and create the css files via autoprefixer and minification
*/
module.exports = function (gulp, $) {

  gulp.task('css', function() {

    return gulp.src(['library/stylesheets/sass/styles.scss'])
      .pipe($.sass()
            .on('error', $.sass.logError)
      )
      .pipe($.autoprefixer({
         browsers: ['last 5 versions'],
         cascade: false
      }))
      .pipe(gulp.dest('library/stylesheets/css'))
      .pipe($.cssnano({
        'safe':true,
        'autoprefixer': false
      }))
      .pipe($.rename('styles.min.css'))
      .pipe(gulp.dest('library/stylesheets/css'))
    
    });
   
};