/*
* Available tasks
* -----------------
* images:minify - Minify images and output to dist/
* images:sprite - Create SVG & PNG sprite
*/
module.exports = function (gulp, $) {

  var pngquant = require('imagemin-pngquant');
  
  /*
  * Create image sprite
  */
  gulp.task('images:sprite', function() {

    var spritePadding = 3;

    return gulp.src(['**/*.svg'], {
          cwd : 'library/images/svg/'
        })
        .pipe($.svgSprite({
          shape : {
            id : {},
            dimension : {
              precision   : 2,
              attributes  : false,
            },
            spacing : {
              padding     : spritePadding,
            },
          },
          mode : {
            /*
            * Standard svg sprite with matching CSS map
            */
            css : {
              dest: 'library/stylesheets/css',
              sprite : '../../images/sprite.svg',
              prefix: '',
              bust : false,
              render: {
                scss: {
                  dest: '../sass/_svg-sprite.scss',
                  template: 'library/stylesheets/sass/_svg-sprite-template.scss',
                }
              }
            }
          },

       

          variables : {
            // Custom variables we use in our svg-sprite-template.scss
            png : function() {
              return function(sprite, render) {
                return render(sprite).split('.svg').join('.png');
              }
            },
            // See this issue https://github.com/jkphl/svg-sprite/issues/134
            // To compensate for your padding value i had to adjust the background-position manually.
            correctPosition: function() {
              return function(backgroundPosition, render) {
                var backgroundPosition = render(backgroundPosition);
                return parseFloat(backgroundPosition - spritePadding)+ "px";
              }
            }
          }
        }))
        .pipe(gulp.dest('.'))
        .pipe($.filter("library/images/sprite/*.svg"))
        .pipe($.raster())
        .pipe($.rename({extname: '.png'}))
        .pipe(gulp.dest('.'));
  });

};
