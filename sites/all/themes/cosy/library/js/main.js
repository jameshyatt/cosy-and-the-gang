function version($log) {
    window.console && console.log($log);
}
function dbg($log) {
    window.console && console.log($log);
}
require.config({
    baseUrl: '/sites/all/themes/cosy/library/js/libs',
    urlArgs: "bust=" + (new Date()).getTime(),
    paths: {
        base: '../',
        jquery: 'jquery-1.11.2.min',
        onmediaquery: 'onmediaquery.min',
        owlcarousel: 'owl.carousel.min',
        stacktable: 'stacktable',
        tingle: 'tingle/tingle',
    }
});

require(['jquery'], function($) {
    $(document).ready(function() {
        require(['base/bootstrap']);
    });
});