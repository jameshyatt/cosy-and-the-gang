define(['tingle'], function(tingle) {

  var modal = new tingle.modal({
      footer: false,
      closeMethods: ['overlay', 'button', 'escape'],
      closeLabel: "Close",
  });

  modal.setContent('<span>Subscribe to our monthly<br>[rare is cool] newsletter.</span><form><input type="text" placeholder="your email" class="subscribe__input"/><input class="subscribe__send" type="submit" value="sign up!"></form>');

  $(".js-subscribe").on("click", function () {
    modal.open();
    return false;
  });

  $('a[href$="http://www.cosyandthegang.com/#newsletter"]').on("click", function () {
    modal.open();
    return false;
  });

});