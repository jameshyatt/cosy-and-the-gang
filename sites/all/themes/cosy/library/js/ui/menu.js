define(function() {

	var pfx = ["webkit", "moz", "MS", "o", ""];

	function PrefixedEvent(element, type, callback) {
		for (var p = 0; p < pfx.length; p++) {
			if (!pfx[p]) type = type.toLowerCase();
			element.addEventListener(pfx[p]+type, callback, false);
		}
	}

	var $menuBtn = null,
		$body = $("body"),
		anim = document.getElementById("cosy-menu");

	return {

		init: function() {
			this.setUp();
		},

		setUp: function() {
			$menuBtn = $(".charms-left .menu");
			this.handleClick();
			anim.addEventListener("webkitAnimationEnd", $.proxy(this.animationEnded, this));
			anim.addEventListener("animationend",  $.proxy(this.animationEnded, this));

			//this.setMenuHeight();
			$(window).on("resize",$.proxy(function() {
				//this.setMenuHeight();
			},this));
		},

		handleClick: function() {
			$menuBtn.on("click",$.proxy(function() {

				$body.toggleClass("menu-open");
				
				if($body.hasClass("menu-open")) {
					$body.addClass("menu-opening");
					$body.removeClass("menu-closing");
				} else {
					$body.addClass("menu-closing");
					$body.removeClass("menu-opening");
					$body.removeClass("menu-opened")

					this.showBody();
				}

				return false;

			},this));
		},

		hideBody: function() {

			$(".page > .main").hide();
			$(".page > footer").hide();

			$(".cosy-menu").css("position","relative");
			$(".cosy-menu").css("z-index","98");
			$(".cosy-menu").css("top","0");

//			this.setMenuHeight();
		},

		showBody: function() {
			$(".page > .main").show();
			$(".page > footer").show();

			$(".cosy-menu").css("position","fixed");
			$(".cosy-menu").css("z-index","");
			$(".cosy-menu").css("top","");

//			$(".cosy-menu").height("auto");
		},

//		setMenuHeight: function() {
//			var h = $(window).height() - 180,
//				minHeight = 650;
//			if(h < minHeight) {
//				h = minHeight;
//			} else {
//				h = h + 1;
//			}
//			$(".cosy-menu").height(h);
//		},

		animationEnded: function() {

			$body.removeClass("menu-closing")

			if($body.hasClass("menu-open")) {
				$body.addClass("menu-opened");

				this.hideBody();

			} else {
				$body.removeClass("menu-opened");
			}

		}

	};
});
