define(['owlcarousel'], function(owl) {

	return {

		init: function() {
			this.setUp();
		},

		setUp: function() {
			this.handleCollectionClick();
			this.preLoadProductImages();
			this.createCarousel();
		},

		handleCollectionClick: function() {

			$(".product-view__thumbnail a").on("click",function() {
				var src = $(this).attr("href");
						   $(".product-view__main-image img").attr("src",src);
				return false;
			});
		},

		preLoadProductImages: function() {
			$(".product-view__thumbnail a").each(function() {
			    var imageObject = new Image();
        		imageObject.src = $(this).attr("href");
			});
		},

		createCarousel: function() {
			/*
			$( ".view-products-by-featured .grid-teaser" ).clone().appendTo( ".view-products-by-featured .grid-with-lines" );
			$(".view-products-by-featured .grid-teaser").unwrap();
			var divs = $(".view-products-by-featured .grid-teaser");
			for(var i = 0; i < divs.length; i+=8) {
			  divs.slice(i, i+8).wrapAll("<div class='grid-with-lines carousel clearfix'></div>");
			}
			*/
			$('.view-products-by-featured').owlCarousel({
				items:1,
				animateOut: 'fadeOut'
			});
		}

	};
});