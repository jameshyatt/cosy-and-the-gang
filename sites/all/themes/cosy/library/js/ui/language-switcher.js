/*
function DropDown(el) {
    this.dd = el;
    this.placeholder = this.dd.children('span');
    this.opts = this.dd.find('ul.dropdown > li');
    this.val = '';
    this.index = -1;
    this.initEvents();
}
DropDown.prototype = {
    initEvents : function() {
        var obj = this;
 
        obj.dd.on('click', function(event){
            $(this).toggleClass('active');
            return false;
        });
 
        obj.opts.on('click',function(){
            var opt = $(this);
            obj.val = opt.text();
            obj.index = opt.index();
            obj.placeholder.text('Gender: ' + obj.val);
        });
    },
    getValue : function() {
        return this.val;
    },
    getIndex : function() {
        return this.index;
    }
}
*/

define(function() {

	var $switcherBlock = $("#language"),
		$switcherList  = $(".language-switcher-locale-url", $switcherBlock),
		selectedLanguage = "Eng";

	return {

		init: function() {
			this.setUp();
		},

		setUp: function() {
			this.getSelectedLanguage();
			this.createDropdown();
            this.handleClick();
		},

		getSelectedLanguage: function() {
			selectedLanguage = $switcherList.find(".active").eq(0).text();
		},

		createDropdown: function() {
			var markup = '<div id="language-dd" class="wrapper-dropdown" tabindex="1"></div>';
			$switcherList.wrap(markup);
			$switcherList.before('<span class="select-language">'+selectedLanguage+'</span>');
            this.closeDowndown();
		},

        closeDowndown: function() {
            $switcherList.hide();
        },

		handleClick: function() {
            $(".select-language").on("click",function() {
                $switcherList.slideToggle();
                $("span.select-language").toggleClass("active");
            });
		}

	};
});
