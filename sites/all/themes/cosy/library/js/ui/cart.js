define(function() {

	return {

		addToBasketValue : "",

		init: function() {
			this.setUp();
		},

		setUp: function() {

			this.checkforHash();
			this.listenForClick();
		},

		listenForClick: function() {
			$(".basket-teaser").on("click",$.proxy(function() {
				this.hideshowBasket();
				return false;
			},this));

			$(".commerce-add-to-cart #edit-submit").on("click",$.proxy(function() {
				this.updateButtonAdding();
			},this));

		},

		hideshowBasket: function() {
				$("#commerce-cart-block-block-1").fadeToggle(300);
				$(".basket-teaser").toggleClass("active");
		},

		checkforHash: function() {
			if(window.location.hash) {
				window.location.hash = '';
				this.hideshowBasket();
				this.delayCloseBasket();
				this.updateButtonAdded();
			}
		},

		delayCloseBasket: function() {
			setTimeout($.proxy(function() {
				this.hideshowBasket();
			},this),4000);
		},

		updateButtonAdded: function() {
			this.addToBasketValue = $("#edit-submit").attr("value");
			$("#edit-submit").attr("value","Added to basket").addClass("added");
			setTimeout($.proxy(function() {
				$("#edit-submit").attr("value",this.addToBasketValue).removeClass("added");
			},this),4000);
		},

		updateButtonAdding: function() {
			$("#edit-submit").attr("value","Adding...").addClass("added");
		}
	};
});