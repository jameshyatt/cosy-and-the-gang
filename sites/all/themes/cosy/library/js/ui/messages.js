define(function() {

	var $message = null;

	return {

		init: function() {
			this.setUp();
		},

		setUp: function() {
			$message = $(".drupal-messages .messages");
			this.handleClick();
			this.autoHide();
		},

		handleClick: function() {
			$message.on("click",function() {
				$(this).parent().addClass("fadeOut");
				return false;
			});

			$(".home-message .close-message").on("click",function() {
				$(".home-message").addClass("hide-message");
				return false;
			});


		},

		autoHide: function() {
			if($message.length > 0) {
				setTimeout(function() {
					$message.parent().addClass("fadeOut");
				},5000);
			}
		}
	};
});