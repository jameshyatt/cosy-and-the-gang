define(['onmediaquery'], function(onmediaquery) {

	var queries = [
	    {
	        context: 'grid-4',
	        match: function() {
		        $(window).trigger("mq:enter-grid-4");
	        },
	        unmatch: function() {
	        	$(window).trigger("mq:leave-grid-4");
	        }
	    },
	    {
	        context: 'grid-6',
	        match: function() {
		        $(window).trigger("mq:enter-grid-6");
	        },
	        unmatch: function() {
	        	$(window).trigger("mq:leave-grid-6");
	        }
	    },
	    {
	        context: 'grid-9',
	        match: function() {
		        $(window).trigger("mq:enter-grid-9");
	        },
	        unmatch: function() {
	        	$(window).trigger("mq:leave-grid-9");
	        }

	    },
	    {
	        context: 'grid-12',
	        match: function() {
		        $(window).trigger("mq:enter-grid-12");
	        },
	        unmatch: function() {
	        	$(window).trigger("mq:leave-grid-12");
	        }

	    }
	],
	$ = jQuery;
	return {
		init: function() {
			this.setUp();
		},

		setUp: function() {
			MQ.init(queries);
		}
	};
});
