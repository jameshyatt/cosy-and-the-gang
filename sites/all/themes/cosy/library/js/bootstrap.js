require(['base/ui/menu',
  'base/ui/cart',
  'base/ui/language-switcher',
  'base/ui/messages',
  'base/ui/global',
  'base/ui/table',
  'base/ui/newsletter',
  'base/framework/media-queries',
  ],
  function(ui__menu,
    ui__cart,
    ui__switcher,
    ui__messasges,
    ui__global,
    ui__table,
    ui__newsletter,
    framework__media) {

    ui__menu.init();
    ui__cart.init();

    ui__switcher.init();
    ui__messasges.init();

    ui__global.init();

    framework__media.init();

  });