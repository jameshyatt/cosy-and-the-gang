define(function() {

	$falseTeaser = '<div class="faux-teaser"><div class="bleed"></div></div>';

	return {

		init: function() {
			this.onMQ();
		},

		onMQ: function() {
			$(window).bind("mq:enter-grid-12",function() {
				$(".collection-spacer").before($falseTeaser)
			});

			$(window).bind("mq:leave-grid-12",function() {
				$(".faux-teaser").remove();
			});
		},

	};
});
