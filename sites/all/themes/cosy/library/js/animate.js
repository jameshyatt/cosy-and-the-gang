define(['skrollr'],function(skrollr) {

	return {
		init: function() {
			this.initSkrollr();
			this.onPageResize();
		},

		initSkrollr: function() {
			if($('html').hasClass('no-touch')) {
				//CSS linked with JS by changing color on HTML
				if($('html').css('color') == 'rgb(18, 52, 86)') {
					var s = skrollr.init({
						easing:'begin/end'
					});
				}
			}
		},

		onPageResize: function() {
			$(window).resize($.proxy(function() {
				this.initSkrollr();
			},this));
		}

	};
});
