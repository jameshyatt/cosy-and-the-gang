define([], function() {

	var $ = jQuery,
		$hoverShare = $('.share-options'),
		$shareLink = $('.share-options a'),
		$twitterLink = $('.twitter'),
		$linkedinLink = $('.linkedin'),

		articleHREF = null,
		articleTitle = null;

		function twitterCount($target, articleHREF) {
		    APIURL = 'http://cdn.api.twitter.com/1/urls/count.json?&url=';
			$.ajax({
				dataType: "jsonp",
				url: APIURL + articleHREF,
				success: function(data) {
					$target.find('.twitter-count').text(data.count);
				}
			});
		}

		function linkedinCount($target, articleHREF) {
			APIURL = 'http://www.linkedin.com/countserv/count/share?url=';
			$.ajax({
				dataType: "jsonp",
				url: APIURL + articleHREF + '&format=jsonp',
				success: function(data) {
					$target.find('.linkedin-count').text(data.count);
				}
			});
		}

	return {

		init: function() {
			this.setUp();
		},

		setUp: function() {

			$hoverShare = $('.share-options');

			if($hoverShare.length == 0) {
				return false;
			}
			//Redeclaring so when pjax runs this script it has the var
			$shareLink = $('.share-options a'),
			$twitterLink = $('.twitter'),
			$linkedinLink = $('.linkedin'),

			articleHREF = null,
			articleTitle = null;

			this.shareLinkClick();
			this.getShareCount();
		},

		shareLinkClick: function() {
			$shareLink.on('click', $.proxy(function(linkClick) {
				this.getArticleInfo(linkClick);
			},this));
		},

		getArticleInfo: function(linkClick) {
			$target = $(linkClick.currentTarget),
			$article = $target.closest($('article'));

			articleHREF = $target.closest('ul').attr('data-href');
			articleTitle = $article.find('.article-title').text(),
			articleContent = $article.find('.entry-content').text();

			this.selectNetwork(linkClick);
		},

		selectNetwork: function(linkClick) {
			if($target.is($twitterLink)) {
				this.twitterShare(linkClick);
			} else if($target.is($linkedinLink)) {
				this.linkedinShare(linkClick);
			}
		},

		twitterShare: function(linkClick) {
			var	twitterMaxLength = 140 - (articleHREF.length + 1);

		    if (articleTitle.length > twitterMaxLength) {
		        articleTitle = articleTitle.substr(0, (twitterMaxLength - 3)) + '...';
		    }

		    var twitterLink = 'https://twitter.com/intent/tweet?url=' + encodeURIComponent(articleHREF) + '&text=' + encodeURIComponent(articleTitle);

		    var width  = 600,
		        height = 400,
		        left   = ($(window).width()  - width)  / 2,
		        top    = ($(window).height() - height) / 2,
		        opts   = 'status=1' +
		                 ',width='  + width  +
		                 ',height=' + height +
		                 ',top='    + top    +
		                 ',resizable=0' + 
		                 ',left='   + left;
		    window.open(twitterLink, 'Twitter', opts);

		},

		linkedinShare: function(linkClick) {
			var linkedinLink = 'http://www.linkedin.com/shareArticle?mini=true&url=' + articleHREF + '&title=' + articleTitle + '&summary=' + articleContent + '&source=Only Studio';

			var width  = 600,
		        height = 400,
		        left   = ($(window).width()  - width)  / 2,
		        top    = ($(window).height() - height) / 2,
		        opts   = 'status=1' +
		                 ',width='  + width  +
		                 ',height=' + height +
		                 ',top='    + top    +
		                 ',left='   + left;
			window.open(linkedinLink, '', opts);
		},

		getShareCount: function() {
			$shareLink.each(function() {
    			$target = $(this);
    			articleHREF = $target.closest('ul').attr('data-href');

    			if($target.is($twitterLink)) {
					twitterCount($target, articleHREF);
				} else if($target.is($linkedinLink)) {
					linkedinCount($target, articleHREF);
				}
			});
		}


	};

});